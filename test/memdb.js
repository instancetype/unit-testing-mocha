/**
 * Created by instancetype on 6/19/14.
 */
var memdb = require('..')
  , assert = require('assert')

describe('memdb', function() {
  beforeEach(function() {
    memdb.clear()
  })

  describe('.save(doc)', function() {
    it('should save the document', function(done) {
      var pet = { name: 'Kalymba' }
      memdb.save(pet, function() {
        var ret = memdb.first({ name: 'Kalymba' })
        assert(ret === pet)
        done()
      })
    })
  })

  describe('.first(obj', function() {
    it('should return first matching doc', function() {
      var jitsu = { name: 'Jitsu' }
        , kalymba = { name: 'Kalymba' }

      memdb.save(jitsu)
      memdb.save(kalymba)

      var ret1 = memdb.first({ name: 'Jitsu' })
      assert(ret1 === jitsu)
      var ret2 = memdb.first({ name: 'Kalymba' })
      assert(ret2 === kalymba)
    })

    it('should return null when no doc matches', function() {
      var ret = memdb.first({ name: 'Ravi' })
      assert(ret === undefined)
    })
  })
})